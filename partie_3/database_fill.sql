CREATE TABLE `personnes` (
  `id_personne` Int PRIMARY KEY,
  `nom` Varchar(50) NOT NULL,
  `prenom` Varchar(50) NOT NULL,
  `email` Varchar(100) NOT NULL,
  `pays_residence` Int NOT NULL
);

CREATE TABLE `voyage` (
  `id_personne` Int NOT NULL,
  `id_pays` Int NOT NULL,
  `duree` Int NOT NULL
);

CREATE TABLE `pays` (
  `id_pays` Int PRIMARY KEY,
  `pays` Varchar(50) NOT NULL
);

ALTER TABLE `personnes` ADD FOREIGN KEY (`pays_residence`) REFERENCES `pays` (`id_pays`);

ALTER TABLE `voyage` ADD FOREIGN KEY (`id_personne`) REFERENCES `personnes` (`id_personne`);

ALTER TABLE `voyage` ADD FOREIGN KEY (`id_pays`) REFERENCES `pays` (`id_pays`);

INSERT INTO `pays` (`id_pays`, `pays`) VALUES 
(1, 'France'),
(2, 'Canada'),
(3, 'Japon'),
(4, 'Brésil'),
(5, 'Allemagne');

INSERT INTO `personnes` (`id_personne`, `nom`, `prenom`, `email`, `pays_residence`) VALUES 
(1, 'Nom1', 'Prenom1', 'email1@exemple.com', 4),
(2, 'Nom2', 'Prenom2', 'email2@exemple.com', 5),
(3, 'Nom3', 'Prenom3', 'email3@exemple.com', 3),
(4, 'Nom4', 'Prenom4', 'email4@exemple.com', 5),
(5, 'Nom5', 'Prenom5', 'email5@exemple.com', 5),
(6, 'Nom6', 'Prenom6', 'email6@exemple.com', 2),
(7, 'Nom7', 'Prenom7', 'email7@exemple.com', 3),
(8, 'Nom8', 'Prenom8', 'email8@exemple.com', 3),
(9, 'Nom9', 'Prenom9', 'email9@exemple.com', 3),
(10, 'Nom10', 'Prenom10', 'email10@exemple.com', 5);

INSERT INTO `voyage` (`id_personne`, `id_pays`, `duree`) VALUES 
(4, 1, 7),
(8, 3, 18),
(8, 3, 4),
(3, 2, 25),
(6, 4, 28),
(5, 4, 14),
(2, 3, 18),
(8, 4, 26),
(6, 4, 9),
(2, 1, 26),
(5, 3, 21),
(1, 5, 2),
(10, 3, 20),
(6, 5, 28),
(9, 1, 15);
