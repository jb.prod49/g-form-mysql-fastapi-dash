# Documentation de l'API

Cette documentation décrit les routes disponibles dans l'API de données de voyage, leurs fonctionnalités et fournit des exemples de formats de réponse.

## Aperçu des Routes

### 1. Données par Pays

- **Endpoint** : `/personne_country/{country}`
- **Méthode** : GET
- **Description** : Cette route permet de récupérer les informations relatives aux personnes en fonction du pays de résidence spécifié dans l'URL. En plus, elle renvoie les informations sur les voyages effectués par ces personnes, classées en deux catégories : les voyages effectués par les résidents du pays spécifié et les voyages ayant comme destination ce pays.
- **Exemple de réponse** :

```json
{
  "recherche par pays de résidence": [
    {
      "prenom": "Prenom1",
      "nom": "Nom1",
      "email": "email1@exemple.com",
      "pays_residence": "Brésil",
      "voyages": [
        {
          "pays": "Allemagne",
          "nb_jour_visite": 2
        }
      ]
    }
  ],
  "recherche par pays de destination": [
    {
      "prenom": "Prenom6",
      "nom": "Nom6",
      "email": "email6@exemple.com",
      "pays_residence": "Canada",
      "voyages": "Brésil",
      "duree": 28
    },
    {
      "prenom": "Prenom5",
      "nom": "Nom5",
      "email": "email5@exemple.com",
      "pays_residence": "Allemagne",
      "voyages": "Brésil",
      "duree": 14
    },
    {
      "prenom": "Prenom8",
      "nom": "Nom8",
      "email": "email8@exemple.com",
      "pays_residence": "Japon",
      "voyages": "Brésil",
      "duree": 26
    },
    {
      "prenom": "Prenom6",
      "nom": "Nom6",
      "email": "email6@exemple.com",
      "pays_residence": "Canada",
      "voyages": "Brésil",
      "duree": 9
    }
  ]
}

```

### 2. Données par Nom de Personne

- **Endpoint** : `/personne_name/{name}`
- **Méthode** : GET
- **Description** : Récupère les données en fonction du nom ou prénom de la personne.
- **Exemple de réponse** :

```json
[
  {
    "prenom": "Prenom6",
    "nom": "Nom6",
    "email": "email6@exemple.com",
    "pays_residence": "Canada",
    "voyages": [
      {
        "pays": "Brésil",
        "duree": 28
      },
      {
        "pays": "Brésil",
        "duree": 9
      },
      {
        "pays": "Allemagne",
        "duree": 28
      }
    ]
  }
]

```

### 3. Statistiques de Voyage

- **Endpoint** : `/stats`
- **Méthode** : GET
- **Description** : Fournit des statistiques comme le pays le plus visité, la durée moyenne des visites par pays et la personne qui a le plus voyagé.
- **Exemple de réponse** :

```json
{
  "pays_plus_visite": {
    "pays": "Japon",
    "nb_visite": 5
  },
  "duree_moyen_par_pays": [
    {
      "pays": "Canada",
      "duree_moyen": 25.0
    },
    {
      "pays": "Brésil",
      "duree_moyen": 19.25
    },
    {
      "pays": "Japon",
      "duree_moyen": 16.2
    },
    {
      "pays": "France",
      "duree_moyen": 16.0
    },
    {
      "pays": "Allemagne",
      "duree_moyen": 15.0
    }
  ],
  "plus_de_voyage": {
    "id_personne": 8,
    "prenom": "Prenom8",
    "nom": "Nom8",
    "nombre_de_voyage": 3
  }
}

```

### 4. Jours de Visite par Pays

- **Endpoint** : `/country/days_visit`
- **Méthode** : GET
- **Description** : Retourne les jours de visite pour chaque pays.
- **Exemple de réponse** :

```json
[
  {
    "pays": "États-Unis",
    "nb_jour_visite": 12
  }
]
```

### 5. Voyage Vers

- **Endpoint** : `/person/travel_to`
- **Méthode** : GET
- **Description** : Récupère les données de voyage pour chaque personne, incluant le pays de résidence et les pays visités avec les jours.
- **Exemple de réponse** :

```json
[
  {
    "nom": "Doe",
    "prenom": "John",
    "pays_residence": "France",
    "voyages": [
      {
        "pays": "Allemagne",
        "nb_jour_visite": 4
      }
    ]
  }
]
```

### 6. Voyages

- **Endpoint** : `/voyages`
- **Méthode** : GET
- **Description** : Récupère des informations détaillées sur les voyages incluant le nom de la personne, la résidence et les détails du voyage.
- **Exemple de réponse** :

```json
[
  {
    "pays": "Italie",
    "nb_jour_visite": 7,
    "nom": "Doe",
    "prenom": "Jane",
    "pays_residence": "France"
  }
]
```

## Démarrer l'API

Pour démarrer l'API, utilisez la commande suivante :

```
uvicorn API:app --reload
```
