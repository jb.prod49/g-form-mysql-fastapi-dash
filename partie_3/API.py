import unicodedata
import pandas as pd
import mysql.connector
from fastapi import FastAPI
from mysql.connector import Error
from config.bdd_config import DB_CONFIG

# uvicorn API:app --reload
app = FastAPI()
# --------------------------------- #
# --------- Connexion BDD --------- #
# --------------------------------- #
def connect_bdd(db_config):
    connect = mysql.connector.connect(**db_config)
    return connect

def close_db_connection(conn: mysql.connector.connect):
    if conn.is_connected():
        conn.close()    

# --------------------------------- #
# --------- Les fonctions --------- #
# --------------------------------- #

def execute_query(query: str, params=None, connect=None, fetch=True):
    try:
        cursor = connect.cursor()
        # print(f"Executing query: {query} with params: {params}")
        cursor.execute(query, params)
        result = cursor.fetchall() if fetch else None
        connect.commit()
        return result
    except Error as e:
        # print(f"Erreur lors de l'exécution de la requête : {e}")
        return None
    
def normalize_string(s):
    return ''.join(c for c in unicodedata.normalize('NFD', s.lower()) if unicodedata.category(c) != 'Mn')

import pandas as pd

def normalize_string(string):
    # Ajoutez ici votre logique de normalisation de chaîne
    return string.lower().strip()

def translate_country(pays_fr):
    df = pd.read_csv('pays.csv', header=None, names=["id", "code", "alpha2", "alpha3", "nom_fr", "nom_en"])
    df['nom_fr'] = df['nom_fr'].apply(normalize_string)
    valeur = normalize_string(pays_fr)
    search = df.loc[df['nom_fr'] == valeur]
    if not search.empty:
        return search['nom_en'].item()
    else:
        return None
    
# ------------------------------------- #
# -------- creation des routes ---------#
# ------------------------------------- #
# Une route qui permet de récupérer les données en fonction du pays.
# http://127.0.0.1:8000/personne_country/{contry}
@app.get("/personne_country/{country}")
def data_by_country(country): 
    #country contient le nom du pays en français
    conn = connect_bdd(DB_CONFIG)
    by_country_residence = []
    query_select = """
                SELECT 
                    pe.id_personne,
                    pe.prenom AS prenom,
                    pe.nom AS nom,
                    pe.email AS email, 
                    pa2.pays AS pays_residence
                FROM
                    personnes pe
                JOIN 
                    voyage vo ON pe.id_personne = vo.id_personne
                JOIN 
                    pays pa ON vo.id_pays = pa.id_pays
                JOIN
                    pays pa2 ON pe.pays_residence = pa2.id_pays
                WHERE
                    pe.pays_residence = (SELECT
                                            id_pays
                                        FROM
                                            pays
                                        WHERE
                                            pays LIKE %s)
                """
    peoples = execute_query(query_select,(country,),conn)
    tab_travel = []
    for people in peoples:
        id_personne = people[0]
        query_select = """
                        SELECT
                            pa.pays,
                            vo.duree
                        FROM
                            voyage vo
                        JOIN
                            pays pa ON vo.id_pays = pa.id_pays
                        WHERE 
                            vo.id_personne = %s
        """
        travel = execute_query(query_select,(id_personne,),conn)
        for x in travel:
            temp = {'pays':x[0],'nb_jour_visite':x[1]}
            tab_travel.append(temp)
        temp = {'prenom': people[1], 'nom':people[2], 'email': people[3],'pays_residence': people[4],'voyages': tab_travel }
        by_country_residence.append(temp)
        
    by_country_travel = []
    query_select = """
                    SELECT
                        pe.id_personne,
                        pe.prenom AS prenom,
                        pe.nom AS nom,
                        pe.email AS email, 
                        pa2.pays AS pays_residence,
                        pa.pays,
                        vo.duree
                    FROM
                        voyage vo
                    JOIN
                        pays pa ON vo.id_pays = pa.id_pays 
                    JOIN
                        personnes pe ON vo.id_personne = pe.id_personne 
                    JOIN
                        pays pa2 ON pe.pays_residence = pa2.id_pays
                    WHERE
                        vo.id_pays = (SELECT
                                            id_pays
                                        FROM
                                            pays
                                        WHERE
                                            pays LIKE %s)
    """
    result = execute_query(query_select,(country,),conn)
    for x in result:
        temp = {'prenom': x[1], 'nom':x[2], 'email': x[3],'pays_residence': x[4],'voyages': x[5], 'duree': x[6]}
        by_country_travel.append(temp)
    close_db_connection
    tab_by_country = {'recherche par pays de résidence': by_country_residence, 'recherche par pays de destination':by_country_travel}
    return tab_by_country

# Une route qui permet de récupérer les données en fonction de la personne.
# http://127.0.0.1:8000/personne_name/{name}}
@app.get("/personne_name/{name}")
def data_by_lastname_firstname(name): 
    #country contient le nom du pays en français
    conn = connect_bdd(DB_CONFIG)
    tab_travel = []
    tableau = []
    query_select = """
                    SELECT DISTINCT
                        pe.id_personne,
                        pe.prenom AS prenom,
                        pe.nom AS nom,
                        pe.email AS email, 
                        pa2.pays AS pays_residence
                    FROM
                        personnes pe
                    JOIN 
                        voyage vo ON pe.id_personne = vo.id_personne
                    JOIN 
                        pays pa ON vo.id_pays = pa.id_pays
                    JOIN
                        pays pa2 ON pe.pays_residence = pa2.id_pays
                    WHERE
                        pe.nom like %s
                    OR
                        pe.prenom like %s
                    """
    result = execute_query(query_select,(name,name),conn)

    for people in result:
        query_select = """
                        SELECT
                            pa.pays,
                            vo.duree
                        FROM
                            voyage vo
                        JOIN
                            pays pa ON vo.id_pays = pa.id_pays
                        WHERE
                            vo.id_personne = %s
        """
        resultat = execute_query(query_select,(people[0],),conn)
        for y in resultat:
            temp = { "pays":y[0], "duree": y[1]}
            tab_travel.append(temp)
        temp2 = {'prenom': people[1], 'nom':people[2], 'email': people[3],'pays_residence': people[4],'voyages': tab_travel}
        tableau.append(temp2)
    close_db_connection
    return tableau

# Une route qui envoie des stats (pays le plus visité, moyenne de la durée des visites par pays, personne ayant le plus voyagé.)
# http://127.0.0.1:8000/stats}
@app.get("/stats")
def stats():
    # pays le plus visité
    conn = connect_bdd(DB_CONFIG)
    query_select = """
                    SELECT
                        pa.pays,
                        COUNT(*)
                    FROM
                        voyage vo
                    JOIN
                        pays pa ON vo.id_pays = pa.id_pays
                    GROUP BY 
                        vo.id_pays 
                    ORDER BY 
                        COUNT(*) DESC
                    LIMIT 1
    """
    plus_visite = execute_query(query_select,(),conn)
    pays_plus_visite = {"pays": plus_visite[0][0], "nb_visite": plus_visite[0][1]}
    
    #  moyenne de la durée des visites par pays,
    tab_duree_avg = []
    query_select = """
                    SELECT
                        pa.pays,
                        AVG(vo.duree)
                    FROM
                        voyage vo
                    JOIN
                        pays pa ON vo.id_pays = pa.id_pays
                    GROUP BY 
                        vo.id_pays
                    ORDER BY 
                        AVG(vo.duree) DESC
    """
    avg_duree = execute_query(query_select,(),conn)
    for x in avg_duree:
        temp = {"pays": x[0], "duree_moyen": x[1]}
        tab_duree_avg.append(temp)
        
    #personne ayant le plus voyagé
    query_select = """
                    SELECT
                        pe.id_personne,
                        pe.prenom,
                        pe.nom,
                        COUNT(vo.id_pays)
                    FROM
                        personnes pe
                    JOIN
                        voyage vo ON pe.id_personne = vo.id_personne
                    GROUP BY
                        vo.id_personne
                    ORDER BY 
                        COUNT(vo.id_pays) DESC
                    LIMIT 1
    """
    plus_voyager = execute_query(query_select,(),conn)
    voyageur = {'id_personne': plus_voyager[0][0],'prenom':plus_voyager[0][1],'nom':plus_voyager[0][2],'nombre_de_voyage':plus_voyager[0][3]}
    resultat = {"pays_plus_visite":pays_plus_visite, 'duree_moyen_par_pays':tab_duree_avg, 'plus_de_voyage':voyageur}
    return resultat


# http://127.0.0.1:8000/country/days_visit
@app.get("/country/days_visit")
def selectPays(): 
    conn = connect_bdd(DB_CONFIG)
    tab = []
    query_select = """
                    SELECT 
                        pa.pays,
                        vo.duree 
                    FROM
                        pays pa
                    JOIN 
                        voyage vo ON pa.id_pays= vo.id_pays
                    """
    result = execute_query(query_select,(),conn)
    close_db_connection
    for x in result :
        country = translate_country(x[0])
        temp = {'pays': country, 'nb_jour_visite': x[1]}
        tab.append(temp)
    return tab

# http://127.0.0.1:8000/person/travel_to
@app.get("/person/travel_to")
def voyageur():
    conn = connect_bdd(DB_CONFIG)
    tab_voyageurs = []
    query_select = """
                    SELECT
                        pe.id_personne,
                        pe.nom,
                        pe.prenom,
                        pa.pays AS pays_residence
                    FROM
                        personnes pe
                    JOIN
                        voyage vo ON pe.id_personne = vo.id_personne
                    JOIN
                        pays pa ON vo.id_pays = pa.id_pays
    """
    personnes = execute_query(query_select,(),conn)
    for pers in personnes :
        id_pers = pers[0]
        query_select = """
                        SELECT
                            pa.pays,
                            vo.duree
                        FROM
                            voyage vo
                        JOIN
                            pays pa ON pa.id_pays = vo.id_pays
                        WHERE
                            vo.id_personne = %s
        """         
        result = execute_query(query_select,(id_pers,),conn)
        voyages = []
        for travel in result:
            temp_travel = {'pays':translate_country(travel[0]),'nb_jour_visite':travel[1]}
            voyages.append(temp_travel)
        temp = {'nom': pers[2], 'prenom':pers[1], 'pays_residence':translate_country(pers[3]),'voyages': voyages }
        tab_voyageurs.append(temp)
    close_db_connection
    return tab_voyageurs

#http://127.0.0.1:8000/voyages
@app.get("/voyages")
def voyage():
    conn = connect_bdd(DB_CONFIG)
    tab = []
    query_select = """
                    SELECT
                        pa.pays,
                        vo.duree,
                        pe.nom,
                        pe.prenom,
                        pe.pays_residence
                    FROM
                        personnes pe
                    JOIN
                        voyage vo ON pe.id_personne = vo.id_personne
                    JOIN
                        pays pa ON vo.id_pays = pa.id_pays
    """
    result = execute_query(query_select,(),conn)
    for x in result:
        temp = {"pays":x[0], "nb_jour_visite":x[1],"nom":x[2], "prenom":x[3],"pays_residence":x[4]}
        tab.append(temp)
    close_db_connection
    return tab
    
#http://127.0.0.1:8000/voyages/{pays_residence}
@app.get("/voyages/{pays_residence}")
def voyages(pays_residence):
    conn = connect_bdd(DB_CONFIG)
    query_select = """
                    SELECT
                        pa2.pays AS pays_de_résidence,
                        pa.pays AS pays_visité,
                        COUNT(vo.id_pays)AS nb_visite
                    FROM
                        voyage vo
                    JOIN
                        personnes pe ON vo.id_personne = pe.id_personne
                    JOIN
                        pays pa ON vo.id_pays = pa.id_pays
                    JOIN
                        pays pa2 ON pe.pays_residence = pa2.id_pays
                    WHERE
                        pa2.pays = %s
                    GROUP BY 
                        pa2.pays,
                        pa.pays
                    ORDER BY 
                        nb_visite DESC
    """
    result = execute_query(query_select,(pays_residence,),conn)
    close_db_connection
    return result
