import re
import numpy as np
import unicodedata
import pandas as pd
import mysql.connector
from mysql.connector import Error
from config.bdd_config import DB_CONFIG,DB_CONFIG1

# --------------------------------- #
# --------- Connexion BDD --------- #
# --------------------------------- #
def connect_bdd(db_config):
    connect = mysql.connector.connect(**db_config)
    return connect

def close_db_connection(conn: mysql.connector.connect):
    if conn.is_connected():
        conn.close()    

# --------------------------------- #
# --------- Les fonctions --------- #
# --------------------------------- #
def get_spreadsheet_as_csv():
    sheet_id = "11_TY-OTM8LryGQ7GNlGvfTDqVaiIBkEj9ge-ZwkZ-qQ"
    print("Merci Nicolas P.")
    return pd.read_csv(f"https://docs.google.com/spreadsheets/d/{sheet_id}/export?format=csv")

def execute_query(query: str, params=None, connect=None, fetch=True):
    try:
        cursor = connect.cursor()
        # print(f"Executing query: {query} with params: {params}")
        cursor.execute(query, params)
        result = cursor.fetchall() if fetch else None
        connect.commit()
        return result
    except Error as e:
        # print(f"Erreur lors de l'exécution de la requête : {e}")
        return None

def nettoyer_nom_pays(nom):
    if pd.isnull(nom) or not isinstance(nom, str):
        return nom  
    nom_sans_crochet = re.sub(r'\[.*?\]\s*', '', nom)
    nom_minuscule = nom_sans_crochet.lower()
    nom_sans_accent = ''.join((c for c in unicodedata.normalize('NFD', nom_minuscule) if unicodedata.category(c) != 'Mn'))
    return nom_sans_accent

def id_pays(nom_pays):
    if pd.isna(nom_pays):
        return None  
    query = """
                SELECT 
                    id_pays 
                FROM 
                    pays 
                WHERE 
                    pays = %s;
    """
    result = execute_query(query,(nom_pays,),conn)
    if result:
        return result[0][0]
    else:
        return None


#-------------------------------------------------#
#-------- exécution du fichier init.sql ----------#
#-------------------------------------------------#
conn = connect_bdd(DB_CONFIG)
init_sql = 'partie_2/init.sql'

with open(init_sql, 'r') as file:
    sql_script = file.read()

sql_statements = sql_script.split(';')

for statement in sql_statements:
     if statement.strip():
        cursor = conn.cursor()
        try:
            cursor.execute(statement)
            conn.commit()
        except mysql.connector.Error as err:
            print(f"Une erreur est survenue: {err}")
        finally:
            cursor.close()
            
#-----------------------------------------#
#-------- récupérer les données ----------#
#-----------------------------------------#
datas = get_spreadsheet_as_csv()
datas.dropna(inplace=True)
datas = datas.drop(columns='[Q2.1] Voyage')
datas = datas.drop(columns='Horodateur')
datas = datas.rename(columns={'[Q1.1] Prénom': 'prenom', '[Q1.2] Nom': 'nom','[Q1.3] Pays de résidence':'residence','[Q3.1] Pays':'destination', '[Q3.2] Durée du voyage': 'duree' })
datas['residence'] = datas['residence'].apply(nettoyer_nom_pays)
datas['destination'] = datas['destination'].apply(nettoyer_nom_pays)
datas['duree'] = pd.to_numeric(datas['duree'], errors='coerce')
datas = datas[np.isfinite(datas['duree'])]
datas['duree'] = datas['duree'].astype(int)

# ---------------------------------------------------- #
# -------- injecter les données dans la BDD ---------- #
# ---------------------------------------------------- #
conn = connect_bdd(DB_CONFIG1)
email = 'email@pardefault.com'
#Inserer les pays
pays_uniques = pd.concat([datas['residence'], datas['destination']]).unique()
pays_uniques = pays_uniques[~pd.isnull(pays_uniques)]
for pays in pays_uniques:
    cursor = conn.cursor()
    query = """
            INSERT INTO pays (pays) VALUES (%s)
           """
    cursor.execute(query, (pays,))
    conn.commit() 
    cursor.close()

#Inserer les personnes et les voyages
for _, data in datas.iterrows():
    #récuperer ID du pays de résidence
    id_residence = id_pays(data['residence'])

    #Inserer les voyages
    cursor = conn.cursor()
    query = """
            INSERT INTO personnes (nom, prenom, email, pays_residence) VALUES (%s, %s, %s, %s);
    """
    cursor.execute(query, (data['nom'],data['prenom'],email,id_residence))
    conn.commit() 
    cursor.close()
    
    query = """
            SELECT LAST_INSERT_ID() FROM personnes
        """
    id_personne = execute_query(query,(),conn)[0][0]


    #Récuperer l'id du pays de destination
    id_destination = id_pays(data['destination'])
    
    #Inserer les voyages
    duree = int(data['duree'])
    cursor = conn.cursor()
    query = """
            INSERT INTO voyage (id_personne, id_pays, duree) VALUES (%s, %s, %s);
    """
    cursor.execute(query, (id_personne,id_destination,int(data['duree'])))
    conn.commit() 
    cursor.close()

close_db_connection(conn)
