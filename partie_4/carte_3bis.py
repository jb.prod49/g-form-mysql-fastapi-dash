import folium
from dash import Dash, dcc, html, Input, Output
import requests 
import pandas as pd 
from pprint import pprint

app = Dash(__name__)

csv_data = open('partie_4/pays-european.csv', 'r')
next(csv_data)
csv_list = []
for row in csv_data:
    row = row.strip().split(',')
    row[1] = float(row[1])
    row[2] = float(row[2])
    csv_list.append(row)

# pprint(csv_list)

def get_data():
    api_url = "http://127.0.0.1:8000/voyages"
    try:
        response = requests.get(api_url)
        response.raise_for_status() 
        data = response.json()
        return data
    except requests.RequestException as e:
        print(f"Error fetching data from API: {e}")
        return None  
    
data = get_data()

# pprint(data)

api_res = []

for i, d in enumerate(data):
    api_res.append({ "origin": d["pays"], "destination": d["pays_residence"]})

# print(api_res)

liste_finale = []

# faire un boucle
# faire une boucle
for pc in api_res:
    origin = next((i for i in csv_list if i[0] == pc["origin"]), None)
    destination = next((i for i in csv_list if i[0] == pc["destination"]), None)
    if origin is not None and destination is not None:
        liste_finale.append({"origin": origin[0], "lat_origin": origin[1], "long_origin": origin[2], "destination": destination[0], "lat_destination": destination[1], "long_destination": destination[2]})

df = pd.DataFrame(liste_finale)

# Create Folium map
m = folium.Map(location=[54.5260, 15.2551], zoom_start=3)

# Add PolyLine between residence and payment points
for index, row in df.iterrows():
    points = [(row['lat_origin'], row['long_origin']), (row['lat_destination'], row['long_destination'])]
    folium.PolyLine(points, color='red', weight=2, opacity=1).add_to(m)

# Add LayerControl
folium.LayerControl().add_to(m)

# Save map to HTML file
m.save('map.html')

# Define Dash layout
app.layout = html.Div([
    html.Iframe(srcDoc=open('map.html', 'r').read(), width='100%', height='600')
])

if __name__ == '__main__':
    app.run_server(debug=True)
