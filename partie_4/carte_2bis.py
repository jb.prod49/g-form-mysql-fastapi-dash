from dash import Dash, dcc, html
import plotly.express as px
import requests 
from dash.dependencies import Input, Output

csv_data = open('partie_4/pays.csv', 'r')
csv_list = []
for i in csv_data:
    i = i.strip().split('\n')
    i = [item.strip('"') for item in i[0].split(',')]
    csv_list.append(i)

# Créer une application Dash
app = Dash(__name__)

def get_data():
    api_url = "http://127.0.0.1:8000/person/travel_to"
    try:
        response = requests.get(api_url)
        response.raise_for_status() 
        data = response.json()
        for pc in data:
            cf = next((i for i in csv_list if i[4] == pc["pays_residence"]), None)
            if cf is not None:
                pc["pays_residence"] = cf[5]
            for voyage in pc["voyages"]:
                country = next((j for j in csv_list if j[4] == voyage["pays"]), None)
                if country is not None:
                    voyage["pays"] = country[5]
        return data
    except requests.RequestException as e:
        print(f"Error fetching data from API: {e}")
        return None

    
data = get_data()


aggregated_data = {}
for person in data:
    residence_country = person['pays_residence']
    for travel in person['voyages']:
        destination_country = travel['pays']
        if destination_country not in aggregated_data:
            aggregated_data[destination_country] = 0
        aggregated_data[destination_country] += 1

print(aggregated_data)

fig = px.choropleth(locations=list(aggregated_data.keys()), 
                    locationmode='country names',
                    color=list(aggregated_data.values()),
                    title='Number of People Travelled to Each Country',
                    width=1200,
                    height=600,
                    color_continuous_scale=px.colors.sequential.Plasma)

# Mise en page de l'application Dash
app.layout = html.Div([
    html.H4('Carte du Continent Européen avec les jours passés par pays par les voyageurs'),
    dcc.Graph(id='europe-map', figure=fig, style={'width': '2000px', 'height': '1000px'})
])

# Exécuter l'application Dash
if __name__ == '__main__':
    app.run_server(debug=True)
