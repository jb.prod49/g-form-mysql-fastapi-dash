from dash import Dash, dcc, html
import plotly.express as px
import geopandas as gpd
import requests
import pandas as pd


# Créer une application Dash
app_dash = Dash(__name__)

def get_data():
    api_url = "http://127.0.0.1:8000/country/days_visit"
    try:
        response = requests.get(api_url)
        response.raise_for_status()
        data = response.json()
        return data
    except requests.RequestException as e:
        print(f"Error fetching data from API: {e}")
        return None

def get_world_geojson():
    world = gpd.read_file(gpd.datasets.get_path('naturalearth_lowres'))
    return world

# Créer la carte
def create_world_map(data):
    world_geojson = get_world_geojson()
    countries = [entry['pays'] for entry in data]
    total_days = [entry['nb_jour_visite'] for entry in data]

    # Create a dictionary to store the data
    data_dict = {'countries': countries, 'total_days': total_days}

    # Create a DataFrame from the dictionary
    df = pd.DataFrame(data_dict)

    # Merge the DataFrame with the world GeoJSON data
    merged_df = pd.merge(world_geojson, df, left_on='name', right_on='countries', how='left')

    fig = px.choropleth(
        merged_df,
        geojson=merged_df.geometry,
        locations=merged_df.index, 
        color='total_days',
        color_continuous_scale='Viridis',
        title='Nombre total de jours passés par pays dans le monde',
        hover_name='name',
        height=600,
        width=1200,
        hover_data={'name': True, 'total_days': True}
    )

    fig.update_geos(
        projection_type="natural earth",
        showcoastlines=True,
        coastlinecolor="white",
        showland=True,
        landcolor="lightgray",
    )

    return fig

# Mise en page de l'application Dash
app_dash.layout = html.Div([
    html.H4('Carte du Monde avec les voyageurs par pays'),
    dcc.Graph(id='world-map', figure=create_world_map(get_data()), style={'width': '2000px', 'height': '1000px'})
])

# Exécuter l'application Dash
if __name__ == '__main__':
    app_dash.run_server(debug=True)
